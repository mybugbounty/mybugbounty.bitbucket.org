#!/bin/bash

cd

echo "set shell to bash"
chsh -s $(which bash)

echo "set time zone"
timedatectl set-timezone Europe/Vienna

echo "set keyboard layout"
localectl set-x11-keymap de pc105

apt update
apt install gedit -y

# in the meantime - edit the configs...
gedit /etc/ssl/openssl.cnf /etc/samba/smb.conf &

apt install sublist3r feroxbuster -y

wget -O bugbounty.zip https://bitbucket.org/mybugbounty/mybugbounty.bitbucket.org/raw/HEAD/bugbounty.zip
unzip -n bugbounty.zip

wget -O zipboxes.sh https://bitbucket.org/mybugbounty/mybugbounty.bitbucket.org/raw/HEAD/scripts/zipboxes.sh
chmod 755 zipboxes.sh

echo "apt update && apt upgrade -y && apt autoremove -y" > update.sh
chmod 755 *.sh
./update.sh

cd /usr/local/bin

wget -O get_tools.sh https://bitbucket.org/mybugbounty/mybugbounty.bitbucket.org/raw/HEAD/scripts/get_tools.sh

chmod 755 *.py *.sh

# remove 'manual' from all wordlists
# for better scan performance
cd /usr/share/wordlists/dirb
echo "delete 'manual', empty lines etc. from dirb/dirbuster wordlists"
for wordlist in $(grep -alw "manual" *.txt)
do
  dos2unix ${wordlist}

  # delete 'manual'
  sed '/^manual$/d' ${wordlist} > ${wordlist}_tmp
  mv ${wordlist}_tmp ${wordlist}
  # delete backslash \
  sed '/\\/d' ${wordlist} > ${wordlist}_tmp
  mv ${wordlist}_tmp ${wordlist}
  # delete empty lines
  sed '/^$/d' ${wordlist} > ${wordlist}_tmp
  mv ${wordlist}_tmp ${wordlist}
done

cd /usr/share/wordlists/dirbuster
for wordlist in $(grep -alw "manual" *.txt)
do
  # delete 'manual'
  sed '/^manual$/d' ${wordlist} > ${wordlist}_tmp
  mv ${wordlist}_tmp ${wordlist}
  # delete empty lines
  sed '/^$/d' ${wordlist} > ${wordlist}_tmp
  mv ${wordlist}_tmp ${wordlist}
done

# wget https://raw.githubusercontent.com/daviddias/node-dirbuster/master/lists/directory-list-2.3-big.txt
# dos2unix directory-list-2.3-big.txt

cd /usr/share/wordlists
gunzip rockyou.txt.gz
# delete empty lines
sed '/^$/d' rockyou.txt > rockyou.txt_tmp
mv rockyou.txt_tmp rockyou.txt

head -n 50000 rockyou.txt > rockyou_50000.txt
gzip rockyou.txt

# generate custom wordlist for dirbusting
echo "generate custom wordlist !!!!!!!!!! check if correctly generated !!!!!!!!!!!!!!!!!"
cat /usr/share/wordlists/dirb/small.txt /usr/share/wordlists/dirb/common.txt /usr/share/wordlists/dirb/big.txt /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt | grep -v " " | sort -u > ./custom.txt

# generate names list
echo "generate name list"
cat /usr/share/wordlists/nmap.lst /usr/share/wordlists/wfuzz/others/names.txt /usr/share/wordlists/dirb/others/names.txt | tr '[:upper:]' '[:lower:]' | sed '/^$/d' | sed '/^[0-9]*$/d' | sort -u > ./names.txt

#apt install seclists -y
ln -s /usr/share/seclists /usr/share/wordlists/seclists

updatedb