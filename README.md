[TOC]

L a b   S e t u p
===========

## **0a. HOST / Windows**
---
joplin notes (https://joplinapp.org/)  
greenshot (https://getgreenshot.org/downloads/)



## **0c. VM / KALI**
---
https://www.offensive-security.com/kali-linux-vm-vmware-virtualbox-image-download/

**edit VM setup - if necessary**  
Network: Bridged or NAT  
RAM: 3072 MB

**Setup kali**  
Login as kali / kali
~~~
sudo su
passwd
reboot
~~~

Log in as root
~~~
wget -O get_tools.sh https://bitbucket.org/mybugbounty/mybugbounty.bitbucket.org/raw/HEAD/scripts/get_tools.sh
chmod 755 ./get_tools.sh
./get_tools.sh
~~~

**install FoxyProxy addon for Firefox**  
https://addons.mozilla.org/en-US/firefox/addon/foxyproxy-standard/  
and configure "Burp"-setting (http, 127.0.0.1:8080)

**Set scroll back history in terminal**  

1. open terminal > right click into windows > Preferences > Appearance > set "Color scheme": WhiteOnBlack

1. open terminal > right click into windows > Preferences > Behavior   > set "History size": 5000

**GEDIT tabs**  
open GEDIT -> menu -> preferences -> editor:  

1. set "Tab with: 4"

1. check "Insert spaces instead of tabs"

**for ssl and nikto**  
~~~
gedit /etc/ssl/openssl.cnf  
~~~
change to TLSv1 and seclevel to 1:
~~~
[system_default_sect]  
MinProtocol = TLSv1  
CipherString = DEFAULT@SECLEVEL=1
~~~

**SMB Conf**  
protocol negotiation failed: NT_STATUS_CONNECTION_DISCONNECTED   
add directly after `[global]` :
~~~
gedit /etc/samba/smb.conf

client min protocol = NT1
~~~

**restart network manager - if needed**  
~~~
systemctl restart NetworkManager
~~~


M e t h o d o l o g y
===========
https://book.hacktricks.xyz/pentesting-methodology  

https://ippsec.rocks/

## **1. Reconnaissance / OSINT**
---

Search for email adresses: hunter.io

Search for various information in Kali: theHarvester  
```
whois <domain>
```

Search for subdomains in Kali: sublist3r

Search for subdomains: OWASP Amass

Search for subdomains: crt.sh

Check list of websites if alive: tomnomnom httprobe

Check website techs: builtwith.com, firefox addon: wappalyzer.com
,in Kali: whatweb, Burp Suite (Target>Site Map>Response)

www.shodan

www.netcraft

## **2. Host discovery & port scan**
### **Host discovery**  
---
~~~
arp-scan -l
~~~
```
netdiscover  
netdiscover -r 172.21.10.0/24
```
~~~
nmap -sn 192.168.1.0/24
~~~

manual ping-sweep  
```
for i in {1..254}; do ping -c 1 192.168.0.$i | grep 'from'; done
```

### **Port scan**  
Various scans like nmap, dirb, wpscan, whatweb in 1 step
```
scan_all.sh $IP  
```
nmap incl. vuln-scan
```
nmap_staged.sh $IP
nmap_staged.sh $IP vuln
```

**TCP** scan
~~~
nmap -T4 -p- -A $IP
~~~

**UDP** scan  
faster mode - only 100 most important ports (-F)
~~~
nmap -sU -F $IP
~~~  

if all UDP ports are filtered (-sV), but **slower**
~~~
nmap -sUV -F $IP
nmap -sUV $IP
~~~  

UDP scan only selected ports
~~~
nmap -sU -A -p 111,137 $IP
~~~

scan for vulnerabilities  
~~~
nmap -p 80,443 --script=auth,brute,discovery,exploit,vuln ${IP}
~~~

**Firewall**  
~~~
--tcp-flags SYN,FIN
~~~

**Portspoofing**  
Too many ports reported as open
~~~
nmap -sF -p- $IP
nmap -sN -p- $IP
nmap -sX -p- $IP
~~~

**Portknocking**  
If nmap shows **SSH filtered**, try to find the sequence  
Sequence e.g. 1356, 6784, 3409
~~~
nmap -Pn --host-timeout 201 --max-retries 0 -p 1356  $IP
nmap -Pn --host-timeout 201 --max-retries 0 -p 6784  $IP
nmap -Pn --host-timeout 201 --max-retries 0 -p 3409  $IP
~~~
See if new ports are open now
~~~
nmap -T5 -p- $IP
~~~
After many failed attempts: Reset the machine! Something may be messed up!

**Other port scans**
~~~
legion
~~~
~~~
netstat -anop |grep LISTENING
~~~
~~~
masscan -p1-65535 --rate 1000 $IP --router-mac 00:0c:29:14:46:33 --interface eth0
~~~

**Other version enumeration**  
Google `launchpad openssh source` or `launchpad apache2 source`, etc. to find out Linux version corresponding to openssh version (=see nmap).

## **3. Pentesting**
https://bitbucket.org/myoscp/myoscp.bitbucket.org/src/master/

## **11. Stuck?**
---
1. Get **EVERY version** number! Do the version numbers tell you anything about the host?

1. Check **EVERY port**! Have you confirmed the service on the port manually and googled all the things (the SSH string, the banner text, the source)? Portknocking?

1. Is there a service that will allow you to enumerate something useful (i.e. usernames) but maybe doesn't make that obvious (e.g. RID brute-force through SMB with crackmapexec or lookupsid.py, SMTP or SSH for usernames)?

1. Searchsploit! Google for CVE's and exploits. Newer distros have most probably misconfiguration vulns instead of kernel / software vulns.

1. Have you used the **BEST wordlist** possible for your tasks (is there a better/bigger directory list? Is there a SecLists cred list for this service?). Try **wfuzz** or manual dirbusting!

1. Have you fuzzed the directories you have found for a) more directories, or b) common filetypes -x php,pl,sh,etc

1. Have you tried some manual testing (MySQL, wireshark inspections)

1. Tried **ALL** default credentials? (google)

1. Have you collected **ALL** the hashes and cracked them?

1. Brute force **ALL** possible cred combinations (users.txt, passwords.txt+rockyou.txt, cewl, patator)

1. Brute force **ALL** possible services (http **and https**, ssh, ftp, weblogin, etc.)

1. Try medusa instead of hydra? (e.g. use medusa for smb!)

1. Have you tried **ALL** SQLI lists? Have you tried SQLI HTTP-Header parameters? Referer, User-Agent...

1. Can you think of a way to find more information: More credentials/users, more URLs, more files, more ports, more access?

1. Exploitation  
Directory traversal for credentials or useful information?  
Check: Shells: reverse / bind / secure / staged/unstaged payloads Php? Php file upload and upload nc to the machine  
Can upload NC?  
PHP File uploader  
Web shell for command execution  
Upload for reverse shells  
Don't forget binaries can be executed too.  
Search up Kernel  
Null byte %00 

1. Try metasploit, owaspZAP

1. Do you need to relax some of the terms used for searching? Instead of v2.8 maybe we check for anything under 3.

1. Do you need a break?

## **12. Further Ressources**
---
**All pentest tools, ressources, etc.**  
https://kalitut.com/penetration-testing-resources/

OSCP - survival guide - commands, methodology:  
https://github.com/wwong99/pentest-notes/blob/master/oscp_resources/OSCP-Survival-Guide.md

WebApp - test mind-map:  
https://www.amanhardikar.com/mindmaps/webapptest.html



## **13. Enumeration**
---
Box Enumeration Results (user/footholds/pivoting before root/admin)
- year of the pig  
- usernames via SMB, brute force website, search bar sanitizes input and encodes with base64 shown via HTTP headers, encode shell with base64 and send with repeater through JSON value for initial access  
- nmap full scan finds IIS webserver port 49663 with same directory as SMB share, they have common link + SMB is writiable leads to aspx webshell  
- ftp server with anonymous login has binary creds file, convert to ASCII and/or unpickle contents of file and format with python leads to initial foothold, lateral movement from ps aux finds user python file to copy and decode with uncompyle6, file has SWX (7321) creds to copy user SSH keys  
- nmap reveals JSONP endpoints, a type of XSS attack that doesnt verify requests, with dirsearch finding login.js page indicating login bypass by setting cooking to SessionToken. Spoofing cookie from login.js page reveals SSH key and username, cracked with ssh2john  
- enum reveals domain controller running kerberos/ldap, usernames found on webserver & kerbrute validates 3/6. SMB required password brute force with custom wordlist using cewl and smbpasswd to change. SMB lists 2 specific shares associated with printers. MSRPC enumdomusers reveals service printer account and enumprinters reveals account password for evil-winrm.  
- dirsearch reveals /content with specific webserver platform name. searchsploit platform reveals method of obtaining creds in .sql file, crack hashed password for login. PHP Code execution in Ads subsection for reverse shell  
- dirsearch shows /panel and /uploads for file upload bypass with PHP file. PHP files blocked but similar PHTML files allowed for reverse shell.  
- SMB enum finds .exe in unique share, nmap finds webserver on port 31337 accepting unauthorized input, testing with nc confirms, analyzing exe wih immunity debug reveals buffer overflow (offset, bad chars, JMP ESP, msfvenom shellcode) for user shell
- FTP server contains .exe and .dll to be extracted and analyzed for buffer overflow system shell  
- dirsearch finds /bin directory with .exe to analyze with immunity debug for user shell  
- wpscan for valid admin username, brute force with rockyou and edit website 404.php theme for initial shell, enum shows local port 8080 running with user creds found in /opt for SSH tunnel, docker escape with brute forcing local webserver and shell with javascript console  
- subversion port 3690 with website revision repo, repo contains alt domain name and .db file with usernames, svn checkout revision 2 for powershell file with website creds, clone repo in Azure DevOps for aspx shell and commit to new domain for initial shell, lateral movement from winPEAS showing mounted W: drive with svn repos, conf directory contains password (valid username in \Users)  
- answer to password reset question in fake employee picture filename, platform found on site with CVE, SMB with creds has .deb of platform, CVE explains to capture user hash with responder by locally running .deb using creds and domain, injecting XSS payload, crack hash for evil-winrm  
- nmap full/UDP scan shows SNMP 161, onesixtyone for community string to use with snmp-check shows username, brute force smb with rockyou for password to use with evil-winrm   
- enum shows likely domain controller with DNS/LDAP/KER, dig shows unsecured dynamic DNS updates to impersonate server, dirsearch finds .pfx certificate & pfx2john extracts password, openssl extracts contents and impersonates key/cert, nsupdate to add DNS record, responder to capture user hash for web powershell console  
- python website icon reveals /account directory that points to /search source code which uses encoded_cookie deserialized with python, initial shell with PoC edited to decode UTF-8 and send GET request with shell cookie, escape docker by SSH tunnel to brute force SSH on host using website usernames  
- SQL injection through dev tools cookie value to write hex encoded cmd PHP shell to website directory, download PHP reverse shell wih cmd for initial access, lateral movement to user with forensic analysis log containing plaintext SSH password  
- hidden web directory with write permissions, upload PHP shell  
- webserver on port 8080, platform & version CVE, public exploit upload nc executable  
- union based SQLi to username and hashed password  
- webserver port 8080 with default creds, command injection with powershell  
- sensitive data (creds) found on SMB samba server, login to webserver to find platform & version CVE for LFI and reverse PHP shell  
- FTP platform & version number revealed from searchsploit to download and mount file system for ssh keys  
- SMB share w/ AD usernames, lateral movement by pivoting with TGT hashes/unique RPC password permissions, download another share with LSASS dumped hashes to pass the hash with evil-winrm  
- port 8080 home webpage displays platform & version CVE for system escalation RCE with PHP reverse shell through command injection  
- brute forcing webserver admin account with hydra http-post-form (login page, request body, error message) leads to platform & version for file upload (shell)  
- showmount port 2049 NFS reveals website backup folder with mount permissions, contents reveal platform & version CVE that needs creds, creds found via strings in .sdf binary database  
- default nmap scripts reveal eternalblue exploit, found public script  
- contact page on home webpage reveals platform & version CVE for public script. unstable shell so send cmd with nc  
- blog on webpage reveals a poem that alludes to a popular culture character, syntax revealed on other post and password on /robots.txt  
- platform found and version found from searching where its located, leads to CVE for SQLi leads to creds  
- creds found via hacked social media in pastebin, brute force pop3 with hydra for valid account, find 2 messages with temp password, one account valid  
- dirsearch reveals platform & CVE (authenticated), googling leads to default creds, specific platform tool & nullbyte article how to get a shell, can upload any file with curl, PHP reverse shell  
- wordpress server, bruteforce with wpscan for user creds, privesc with searchsploit wordpress privesc for profile update with ure_other_roles=admininstrator  
- website wih .pcap file, multiple connections from client, knock ports with knock && nmap, knock again for now open port leads to hidden directory, repeats process for hidden message with next port numbers spaced??, repeat process leads to knock && ssh revealing creds, ssh creds /bin/sh  
- unique subdirectory with LFI prereqs, new technique requires keyword to read, attempting to read system file appends with .php extension and include() function leads to PHP filter LFI to decode source code which reveals way to read system files and log poisoning to upload shell  
- wordpress server wpscan bruteforce for creds, edit obscure PHP extension for PHP reverse shell  
- webserver platform & version number reveal CVE SQLi public python script that returns hash with salt  
- dirsearch webserver reveal sitemap subdirectory with hidden ssh folder with id_rsa keys, username found in homepage source code  
- webserver platform & version leads to CVE reverse shell  